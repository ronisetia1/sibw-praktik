<?php

/**
 *
 */
class Kategori
{
	protected $db;

	public function __construct($db)
	{
		$this->db = $db;
	}

	public function index()
	{
        $query = "SELECT * FROM produk_kategori";
        $result = $this->db->query($query);

        $data = array();
        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            $data[] = $row;
        }

        echo json_encode([
        	'success' => true,
        	'data' => $data
        ]);
	}

	public function store($data)
	{
		$huruf_pertama = strtoupper(substr($data['kategori_nama'], 0, 3));
		$kategori_kode = $huruf_pertama . date('YmdHis');
		$kategori_nama = $data['kategori_nama'];

		try {
            $query = "INSERT INTO produk_kategori (kategori_kode, kategori_nama) VALUES ('$kategori_kode', '$kategori_nama')";
            $result = $this->db->query($query);

            echo json_encode([
            	'success' => true,
            	'message' => 'Kategori created!'
            ]);
        } catch (\Throwable $th) {
            print_r($th);
            die;
        }
        die();
	}

	public function edit($data)
	{
		$id     = $data['id'];
		$query  = "SELECT * FROM produk_kategori WHERE id = $id";
		$result = $this->db->query($query);

        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            echo json_encode([
				'success' => true,
				'data' => $row
			]);
        }
	}

	public function update($data)
	{
		$id            = $data['id'];
		$kategori_nama = $data['kategori_nama'];

		try {
            $query = "UPDATE produk_kategori SET kategori_nama='$kategori_nama' WHERE id='$id'";
            $result = $this->db->query($query);

            echo json_encode([
            	'success' => true,
            	'message' => 'Kategori updated!'
            ]);
        } catch (\Throwable $th) {
            print_r($th);
            die;
        }
        die();
	}

	public function delete($data)
	{
		$id = $data['id'];

		try {
            $query = "DELETE FROM produk_kategori WHERE id='$id'";
            $result = $this->db->query($query);

            echo json_encode([
            	'success' => true,
            	'message' => 'Kategori deleted!'
            ]);
        } catch (\Throwable $th) {
            print_r($th);
            die;
        }
        die();
	}

    public function search($data)
    {
        $text = $data['text'];

        $query = "SELECT * FROM produk_kategori WHERE kategori_nama LIKE '%$text%' OR kategori_kode LIKE '%$text%'";
        $result = $this->db->query($query);

        $data = array();
        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            $data[] = $row;
        }

        echo json_encode([
            'success' => true,
            'data' => $data
        ]);
    }
}