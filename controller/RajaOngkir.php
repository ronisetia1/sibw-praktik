<?php
    error_reporting(0);

    switch ($_GET['q']) {
        case 'kotaasal':
            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_URL => "http://api.rajaongkir.com/starter/city",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => [
                    "key: 841945b05db799916afdfaf4f96aa3aa"
                ]
            ]);
            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            $data = json_decode($response, true);
            for ($i=0; $i < count($data['rajaongkir']['results']); $i++) {
                echo "<option></option>";
                echo "<option value='".$data['rajaongkir']['results'][$i]['city_id']."'>".$data['rajaongkir']['results'][$i]['city_name']."</option>";
            }
        break;
        case 'kotatujuan':
            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_URL => "http://api.rajaongkir.com/starter/city",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => [
                    "key: 841945b05db799916afdfaf4f96aa3aa"
                ]
            ]);

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            $data = json_decode($response, true);
            for ($i=0; $i < count($data['rajaongkir']['results']); $i++) {
                echo "<option></option>";
                echo "<option value='".$data['rajaongkir']['results'][$i]['city_id']."'>".$data['rajaongkir']['results'][$i]['city_name']."</option>";
            }
        break;
    }

    if (!empty($_POST)) {
        $kota_asal = $_POST['kota_asal'];
        $kota_tujuan = $_POST['kota_tujuan'];
        $kurir = $_POST['kurir'];
        $berat = $_POST['berat']*1000;
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => "http://api.rajaongkir.com/starter/cost",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS =>
            "origin=".$kota_asal."&destination=".$kota_tujuan."&weight=".$berat."&courier=".$kurir."",
            CURLOPT_HTTPHEADER => [
                "content-type: application/x-www-form-urlencoded",
                "key: 841945b05db799916afdfaf4f96aa3aa"
            ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        $data = json_decode($response, true);

        $kurir = $data['rajaongkir']['results'][0]['name'];
        $kotaasal = $data['rajaongkir']['origin_details']['city_name'];
        $provinsiasal = $data['rajaongkir']['origin_details']['province'];
        $kotatujuan = $data['rajaongkir']['destination_details']['city_name'];
        $provinsitujuan = $data['rajaongkir']['destination_details']['province'];
        $berat = $data['rajaongkir']['query']['weight'] / 1000;
        $tbody = '';

        foreach ($data['rajaongkir']['results'][0]['costs'] as $value) {
            $tbody .= "<tr>";
            $tbody .= "<td>" . $value['service'] . "</td>";
            foreach ($value['cost'] as $tarif) {
                $tbody .= "<td align='right'>Rp " . number_format($tarif['value'],2,',','.') . "</td>";
                $tbody .= "<td>" . $tarif['etd'] . " Hari</td>";
            }
            $tbody .= "</tr>";
        }

        echo
            "<div class='panel panel-default'>
                <div class='panel-body'>
                    <table width='100%'>
                        <tr>
                            <td width='15%'><b>Kurir</b> </td>
                            <td>&nbsp;<b><?=$kurir?></b></td>
                        </tr>
                        <tr>
                            <td>Dari</td>
                            <td>: " . $kotaasal . ", " . $provinsiasal . "</td>
                        </tr>
                        <tr>
                            <td>Tujuan</td>
                            <td>: " . $kotatujuan . ", " . $provinsitujuan . "</td>
                        </tr>
                        <tr>
                            <td>Berat (Kg)</td>
                            <td>: " . $berat . "</td>
                        </tr>
                    </table>
                    <br>
                    <table class='table table-striped table-bordered '>
                        <thead>
                            <tr>
                                <th>Nama Layanan</th>
                                <th>Tarif</th>
                                <th>Estimasi pengiriman</th>
                            </tr>
                        </thead>
                        <tbody>" . $tbody . "</tbody>
                    </table>
                </div>
            </div>";
        exit(); die();
    }
