<!-- start: Slider -->
<div class="slider-wrapper">
    <?php include_once './content/slider.php';?>
</div>
<!-- end: Slider -->
<!--start: Wrapper-->
<div id="wrapper">
    <!--start: Container -->
    <div class="container">
        <!-- start: Row -->
        <div class="row" id="rowProduk">
        </div>
        <!-- end: Row -->
    </div>
    <!--end: Container-->
</div>
<!-- end: Wrapper  -->
<script>
    getProduk = () => {
        $.ajax({
            type: 'GET',
            url: 'controller/Ajax.php',
            data: {
                form: 'get_produk',
            },
            dataType: "json",
            success: (res) => {
                if (res.data.length != 0) {
                    res.data.forEach((val, i) => {
                        let el = `<div class="span4">
                                        <div class="icons-box">
                                            <div class="title"><h3>${val.br_nm}</h3></div>
                                            <img src="http://localhost/kuliah/SIBW/3183111047-RoniSetiawan/admin/${val.br_gbr}"/>
                                            <div><h3>Rp. ${val.br_hrg}</h3></div>
                                            <div class="clear">
                                                <a href="index.php?link=detailbarang&kd=${val.br_id}" class="btn btn-lg btn-danger">Detail</a>
                                                <a href="cart.php?act=add&amp;barang_id=${val.br_id}&amp;ref=index.php?link=keranjang&kd=${val.br_id}" class="btn btn-lg btn-success">Beli &raquo;</a>
                                            </div>
                                        </div>
                                    </div>`

                        $('#rowProduk').append(el)
                    })
                } else {
                    let el = `<tr>
                                <td colspan="4" class="text-center">Belum ada produk!</td>
                            </tr>`

                    $('#rowProduk').append(el)
                }
            },
            error: (err) => {
                console.log(err)
            }
        })
    }

    $(document).ready(() => {
        getProduk()
    })
</script>