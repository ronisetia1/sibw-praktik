<div class="dflex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
	<h1 class="h2">Validation</h1>
</div>
<div id="message"></div>
<div class="col-lg-12">
	<input type="search" class="form-control-sm py-3 border mb-3" value="" placeholder="Search..." id="textSearch" name="search">

	<div class="table-responsive">
		<table class="table table-hover">
			<thead class="thead-light">
				<tr>
					<th>No</th>
					<th>No Invoice</th>
					<th>Tgl</th>
					<th>Pembeli</th>
					<th>No. Rek.</th>
					<th>Nama Rek.</th>
					<th>Bank</th>
					<th>Total</th>
					<th>Status</th>
					<th style="text-align: center;">Action</th>
				</tr>
			</thead>
			<tbody id="tbody"></tbody>
		</table>
	</div>
</div>
<script>
	getValidation = () => {
		$.ajax({
            type: 'GET',
            url: 'controller/Ajax.php',
            data: {
                form: 'get_validation',
            },
            dataType: 'json',
            success: (res) => {
            	if (res.data.length != 0) {
	            	res.data.forEach((val, i) => {
	            		let el = `<tr>
									<td>${i + 1}</td>
									<td>${val.no_pem}</td>
									<td>${val.tgl_pem}</td>
									<td>${val.usr_pem}</td>
									<td>${val.norek_pem}</td>
									<td>${val.nmrek_pem}</td>
									<td>${val.bankrek_pem}</td>
									<td>${val.tot_pem}</td>
									<td>${val.sts_pem}</td>
									<td>
										<div class="btn-group">
											<button class="btn btn-success btn-sm" onclick="statusUp(${val.id}, '${val.sts_pem}')"><i class="fas fa-arrow-up text-white"></i></button>
											<button class="btn btn-danger btn-sm" onclick="deleteValidation(${val.id})" disabled><i class="fas fa-trash"></i></button>
										</div>
									</td>
								</tr>`

						$('#tbody').append(el)
	            	})
	            } else {
	            	let el = `<tr>
								<td colspan="10" class="text-center">Belum ada pembelian!</td>
							</tr>`

					$('#tbody').append(el)
	            }
            },
            error: (err) => {
            	console.log(err)
            }
        })
	}

	// fungsi search
	search = (text) => {
		$.ajax({
            type: 'GET',
            url: 'controller/Ajax.php',
            data: {
            	text: text,
                form: 'search_validation',
            },
            dataType: 'json',
            success: (res) => {
            	$('#tbody').empty()
            	if (res.data.length != 0) {
	            	res.data.forEach((val, i) => {
	            		let el = `<tr>
									<td>${i + 1}</td>
									<td>${val.no_pem}</td>
									<td>${val.tgl_pem}</td>
									<td>${val.usr_pem}</td>
									<td>${val.norek_pem}</td>
									<td>${val.nmrek_pem}</td>
									<td>${val.bankrek_pem}</td>
									<td>${val.tot_pem}</td>
									<td>${val.sts_pem}</td>
									<td>
										<div class="btn-group">
											<button class="btn btn-success btn-sm" onclick="statusUp(${val.id}, '${val.sts_pem}')"><i class="fas fa-arrow-up text-white"></i></button>
											<button class="btn btn-danger btn-sm" onclick="deleteValidation(${val.id})" disabled><i class="fas fa-trash"></i></button>
										</div>
									</td>
								</tr>`

						$('#tbody').append(el)
	            	})
	            } else {
	            	let el = `<tr>
								<td colspan="10" class="text-center">Member yang dicari tidak ada!</td>
							</tr>`

					$('#tbody').append(el)
	            }
            },
            error: (err) => {
            	console.log(err)
            }
        })
	}

	statusUp = (id, status) => {
		console.log(id, status)
		$.ajax({
	        type: 'POST',
	        url: 'controller/Ajax.php',
	        data: {
            	id: id,
            	status: status,
                form: 'status_up_validation',
            },
	        dataType: 'json',
	        success: (res) => {
	        	// buat elemen
	        	let el = `<div class="alert alert-success alert-dismissible fade show" role="alert">
							  	<strong>${res.message}</strong>
							  	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							    	<span aria-hidden="true">&times;</span>
							  	</button>
							</div>`

				// kosongkan isi elemen tbody
				$('#tbody').empty()

				// panggil fungsi untuk get data
				getValidation()

				// tampilkan pesan / alert
	        	$('#message').append(el)

	        	// hapus alert
	        	setTimeout(() => {
	        		$('#message').empty()
	        	}, 3000)
	        },
	        error: (err) => {
	        	console.log(err)
	        }
	    })
	}

	// fungsi delete
	deleteValidation = (id) => {
		$.ajax({
            type: 'POST',
            url: 'controller/Ajax.php',
            data: {
            	id: id,
                form: 'delete_validation',
            },
            dataType: 'json',
            success: (res) => {
            	// buat elemen
	        	let el = `<div class="alert alert-success alert-dismissible fade show" role="alert">
							  	<strong>${res.message}</strong>
							  	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							    	<span aria-hidden="true">&times;</span>
							  	</button>
							</div>`

				// kosongkan isi elemen tbody
				$('#tbody').empty()

				// panggil fungsi untuk get data
				getMember()

				// tampilkan pesan / alert
	        	$('#message').append(el)

	        	// hapus alert
	        	setTimeout(() => {
	        		$('#message').empty()
	        	}, 3000)
            },
            error: (err) => {
            	console.log(err)
            }
        })
	}

	// ketika halaman siap,
	$(document).ready(() => {
		// panggil fungsi ini
		getValidation()

		// search
		$('#textSearch').on('keyup', (e) => {
			search(e.target.value)
		})
	})
</script>