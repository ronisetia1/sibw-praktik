<ul class="nav flex-column">
	<li class="nav-item">
		<a href="home.php" class="nav-link active">
			<span><i class="fas fa-home fa-lg"></i></span>
			Dashboard
		</a>
	</li>
	<li class="nav-item">
		<a href="home.php?link=produk" class="nav-link">
			<span><i class="fas fa-shopping-cart fa-lg"></i></span>
			Products
		</a>
	</li>
	<li class="nav-item">
		<a href="home.php?link=kategori" class="nav-link">
			<span><i class="fas fa-layer-group fa-lg"></i></span>
			Category
		</a>
	</li>
	<li class="nav-item">
		<a href="home.php?link=member" class="nav-link">
			<span><i class="fas fa-users fa-lg"></i></span>
			Member
		</a>
	</li>
	<li class="nav-item">
		<a href="home.php?link=validasi" class="nav-link">
			<span><i class="fas fa-money-bill fa-lg"></i></span>
			Validasi
		</a>
	</li>
	<li class="nav-item">
		<a href="logout.php" class="nav-link">
			<span><i class="fa fa-sign-out-alt fa-lg"></i></span>
			Log Out
		</a>
	</li>
</ul>