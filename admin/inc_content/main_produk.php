<div class="modal fade" id="addData" tabindex="-1" aria-labelledby="addDataLabel" aria-hidden="true">
  	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header">
        		<h5 class="modal-title" id="exampleModalLabel">Tambah data</h5>
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          			<span aria-hidden="true">&times</span>
        		</button>
      		</div>
      		<form id="formData" action="#">
      			<div class="modal-body">
			        <div class="row">
			            <div class="col-md-6">
			                <div class="form-group">
				      			<div id="divCrud">
				      				<input type="hidden" id="inputCrud" value="add">
				      			</div>
				      			<div id="divProdukId"></div>
			                    <input type="hidden" id="fileName" name="fileName">
			                    <input type="text" id="produkName" name="produkName" placeholder="Nama Produk" maxlength="20" class="form-control">
			                </div>
			            </div>
			            <div class="col-md-6">
			                <div class="form-group">
			                    <select id="kategoriKode" name="kategoriKode" class="form-control">
			                    </select>
			                </div>
			            </div>
			        </div>
			        <div class="row">
			            <div class="col-md-6">
			                <div class="form-group">
			                    <input type="number" id="harga" name="harga" placeholder="Harga" maxlength="10" class="form-control" min="1">
			                </div>
			            </div>
			            <div class="col-md-6">
			                <div class="form-group">
			                    <input type="number" id="stok" name="stok" placeholder="Stok" maxlength="3" class="form-control" min="1">
			                </div>
			            </div>
			            <div class="col-md-12">
			                <div class="form-group">
			                	<textarea name="keterangan" id="keterangan" class="form-control" placeholder="Keterangan"></textarea>
			                </div>
			            </div>
			            <div class = "col-md-12">
			                <div class = "form-group">
			                    <input type="file" id="produkImage" name="produkImage" placeholder="File" onchange="readURL(this)">
			                    <input type="text" id="imageOld" name="imageOld" class="d-none">
			                </div>
			            </div>
			            <div class = "col-md-6">
			                <div class = "form-group">
			                    <img id="previewProduk" src="#" alt="" width="200px" onclick="$('#produkImage').trigger('click') return false" />
			                </div>
			            </div>
			        </div>
			    </div>
	      		<div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			        <button type="submit" class="btn btn-primary">Submit</button>
	      		</div>
		    </form>
      	</div>
    </div>
</div>
<div class="card">
	<div class="card-header">
		Data Produk
		<div class="float-right">
			<div class="input-group mb-3">
			  	<div class="input-group-prepend">
			      	<button class="btn btn-success btn-sm d-inline" id="btnTambah"><i class="fas fa-plus"></i></button>
			  	</div>
			  	<input type="text" class="form-control" aria-label="Text input with checkbox" id="textSearch" style="outline: none" placeholder="Search">
			</div>
		</div>
	</div>
	<div class="card-body">
		<div id="message"></div>
		<div class="table-responsive">
			<table class="table table-hover table-striped">
				<thead>
					<th style="border-top: none">No</th>
					<th style="border-top: none">Kategori</th>
					<th style="border-top: none">Nama</th>
					<th style="border-top: none">Harga</th>
					<th style="border-top: none">Stok</th>
					<th style="border-top: none">Aksi</th>
				</thead>
				<tbody id="tbody"></tbody>
			</table>
		</div>
	</div>
</div>

<script>
	// function untuk mengambil semua data kategori
	getProduk = () => {
		$.ajax({
            type: 'GET',
            url: 'controller/Ajax.php',
            data: {
                form: 'get_produk',
            },
            dataType: "json",
            success: (res) => {
            	if (res.data.length != 0) {
	            	res.data.forEach((val, i) => {
	            		let el = `<tr>
									<td>${i + 1}</td>
									<td>${val.kategori_nama}</td>
									<td>${val.br_nm}</td>
									<td>${val.br_hrg}</td>
									<td>${val.br_stok}</td>
									<td>
										<div class="btn-group">
											<button class="btn btn-warning btn-sm" onclick="getProductById(${val.br_id})"><i class="fas fa-edit text-white"></i></button>
											<button class="btn btn-danger btn-sm" onclick="deleteProduct(${val.br_id}, '${val.br_gbr}')"><i class="fas fa-trash"></i></button>
										</div>
									</td>
								</tr>`

						$('#tbody').append(el)
	            	})
	            } else {
	            	let el = `<tr>
								<td colspan="4" class="text-center">Belum ada produk!</td>
							</tr>`

					$('#tbody').append(el)
	            }
            },
            error: (err) => {
            	console.log(err)
            }
        })
	}

	// function untuk mengambil semua data kategori
	getKategori = (kode = '') => {
		$.ajax({
            type: 'GET',
            url: 'controller/Ajax.php',
            data: {
                form: 'get_kategori',
            },
            dataType: "json",
            success: (res) => {
            	$('#kategoriKode').empty()
            	const optionDef = `<option value="">Pilih Kategori</option>`
            	$('#kategoriKode').append(optionDef)

            	if (res.data.length != 0) {
	            	res.data.forEach((val, i) => {
	            		let el = `<option value="${val.kategori_kode}" data-id-kategori="${val.id}">${val.kategori_nama}</option>`

						$('#kategoriKode').append(el)
	            	})

	            	if (kode) $('#kategoriKode').val(kode)
	            } else {
	            	let el = `<option value="">Kategori tidak tersedia!</option>`

					$('#kategoriKode').append(el)
	            }
            },
            error: (err) => {
            	console.log(err)
            }
        })
	}

	// function untuk menambah data
	insertProduk = () => {
		let formData = new FormData()

		formData.append('form', 'add_produk')
		formData.append('br_nm', $('#produkName').val())
		formData.append('br_hrg', $('#harga').val())
		formData.append('br_stok', $('#stok').val())
		formData.append('ket', $('#keterangan').val())
		formData.append('br_kat', $('#kategoriKode').val())
		formData.append('image', $('#produkImage')[0].files[0])

		$.ajax({
	        type: 'POST',
	        url: 'controller/Ajax.php',
	        data: formData,
            mimeTypes: "multipart/form-data",
            contentType: false,
            processData: false,
	        dataType: "json",
	        success: (res) => {
	        	// buat elemen
	        	let el = `<div class="alert alert-success alert-dismissible fade show" role="alert">
							  	<strong>${res.message}</strong>
							  	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							    	<span aria-hidden="true">&times;</span>
							  	</button>
							</div>`

				// kosongkan isi elemen tbody
				$('#tbody').empty()

				// panggil fungsi untuk get data
				getProduk()

				// tampilkan pesan / alert
	        	$('#message').append(el)

	        	reset()

	        	// tutup modal
	        	$('#addData').modal('hide')

	        	// hapus alert
	        	setTimeout(() => {
	        		$('#message').empty()
	        	}, 3000)
	        },
	        error: (err) => {
	        	console.log(err.message)
	        }
	    })
	}

	getProductById = (id) => {
		$.ajax({
            type: 'GET',
            url: 'controller/Ajax.php',
            data: {
            	id: id,
                form: 'get_produk_by_id',
            },
            dataType: "json",
            success: (res) => {
            	$('#addData').modal('show')

            	getKategori(res.data.br_kat)

		        $('#produkName').val(res.data.br_nm)
		        $('#harga').val(res.data.br_hrg)
		        $('#stok').val(res.data.br_stok)
		        $('#keterangan').val(res.data.ket)
		        $('#imageOld').val(res.data.br_gbr)
		        $('#kategoriKode').val(res.data.br_kat)
		        $('#previewProduk').attr('src', `http://localhost/kuliah/SIBW/3183111047-RoniSetiawan/admin/${res.data.br_gbr}`)

            	let elId = `<input type="hidden" class="form-control" id="inputProdukId" aria-describedby="id" value="${res.data.br_id}">`
            	let elCr = `<input type="hidden" id="inputCrud" value="edit">`

            	$('#divProdukId').empty()
            	$('#divProdukId').append(elId)
            	$('#divCrud').empty()
            	$('#divCrud').append(elCr)
            },
            error: (err) => {
            	console.log(err)
            }
        })
	}

	updateProduk = () => {
		let formData = new FormData()

		formData.append('form', 'edit_produk')
		formData.append('id', $('#inputProdukId').val())
		formData.append('br_nm', $('#produkName').val())
		formData.append('br_hrg', $('#harga').val())
		formData.append('br_stok', $('#stok').val())
		formData.append('ket', $('#keterangan').val())
		formData.append('br_kat', $('#kategoriKode').val())
		formData.append('image_old', $('#imageOld').val())
		formData.append('image', $('#produkImage')[0].files[0])

		$.ajax({
	        type: 'POST',
	        url: 'controller/Ajax.php',
	        data: formData,
            mimeTypes: "multipart/form-data",
            contentType: false,
            processData: false,
	        dataType: "json",
	        success: (res) => {
	        	// buat elemen
	        	let el = `<div class="alert alert-success alert-dismissible fade show" role="alert">
							  	<strong>${res.message}</strong>
							  	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							    	<span aria-hidden="true">&times;</span>
							  	</button>
							</div>`

				// kosongkan isi elemen tbody
				$('#tbody').empty()

				// panggil fungsi untuk get data
				getProduk()

				// tampilkan pesan / alert
	        	$('#message').append(el)

	        	// tutup modal
	        	$('#addData').modal('hide')

	        	// hapus alert
	        	setTimeout(() => {
	        		$('#message').empty()
	        	}, 3000)
	        },
	        error: (err) => {
	        	console.log(err)
	        }
	    })
	}

    readURL = (input) => {
        if (input.files && input.files[0]) {
            var reader = new FileReader()
            reader.onload = function (e) {
                $('#previewProduk').attr('src', e.target.result)
            }
            reader.readAsDataURL(input.files[0])
        }
    }

    reset = () => {
        $('#produkName').val('')
        $('#harga').val('')
        $('#stok').val('')
        $('#keterangan').val('')
        $('#produkImage').val('')
        $('#kategoriKode').val('')
        $('#previewProduk').attr('src', '')
    }

	// fungsi delete
	deleteProduct = (id, br_gbr) => {
		$.ajax({
            type: 'POST',
            url: 'controller/Ajax.php',
            data: {
            	id: id,
            	br_gbr: br_gbr,
                form: 'delete_produk',
            },
            dataType: "json",
            success: (res) => {
            	// buat elemen
	        	let el = `<div class="alert alert-success alert-dismissible fade show" role="alert">
							  	<strong>${res.message}</strong>
							  	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							    	<span aria-hidden="true">&times;</span>
							  	</button>
							</div>`

				// kosongkan isi elemen tbody
				$('#tbody').empty()

				// panggil fungsi untuk get data
				getProduk()

				// tampilkan pesan / alert
	        	$('#message').append(el)

	        	// hapus alert
	        	setTimeout(() => {
	        		$('#message').empty()
	        	}, 3000)
            },
            error: (err) => {
            	console.log(err)
            }
        })
	}

	// fungsi search
	search = (text) => {
		$.ajax({
            type: 'GET',
            url: 'controller/Ajax.php',
            data: {
            	text: text,
                form: 'search_produk',
            },
            dataType: "json",
            success: (res) => {
            	$('#tbody').empty()
            	if (res.data.length != 0) {
	            	res.data.forEach((val, i) => {
	            		let el = `<tr>
									<td>${i + 1}</td>
									<td>${val.kategori_nama}</td>
									<td>${val.br_nm}</td>
									<td>${val.br_hrg}</td>
									<td>${val.br_stok}</td>
									<td>
										<div class="btn-group">
											<button class="btn btn-warning btn-sm" onclick="getProductById(${val.br_id})"><i class="fas fa-edit text-white"></i></button>
											<button class="btn btn-danger btn-sm" onclick="deleteProduct(${val.br_id}, '${val.br_gbr}')"><i class="fas fa-trash"></i></button>
										</div>
									</td>
								</tr>`

						$('#tbody').append(el)
	            	})
	            } else {
	            	let el = `<tr>
								<td colspan="4" class="text-center">Produk yang dicari tidak ada!</td>
							</tr>`

					$('#tbody').append(el)
	            }
            },
            error: (err) => {
            	console.log(err)
            }
        })
	}

	// ketika halaman siap,
	$(document).ready(() => {
		// panggil fungsi ini
		getProduk()

		// ketika button tambah di klik
		$('#btnTambah').on('click', (e) => {
            let elCr = `<input type="hidden" id="inputCrud" value="add">`

            reset()

            $('#divProdukId').empty()
            $('#divCrud').empty()
            $('#divCrud').append(elCr)
	        $('#addData').modal('show')

			getKategori()
		})

		// ambil event ketika form di submit
		$('#formData').on('submit', (e) => {
			e.preventDefault()

			let crud = $('#inputCrud').val()

			if (crud == 'add') {
				// panggil fungsi insert
				insertProduk()

				reset()
			} else if (crud == 'edit') {
				// panggil fungsi update
				updateProduk()
			}
		})

		// search
		$('#textSearch').on('keyup', (e) => {
			search(e.target.value)
		})
	})
</script>