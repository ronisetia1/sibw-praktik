<?php
	session_start();

	if (isset($_SESSION['gagal'])) {
		if ($_SESSION['gagal'] >= 3) {
			echo '<h1>ANDA SEDANG DIBLOKIR</h1>';
			exit();
		}
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>LOGIN - SIBW</title>
	<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
</head>
<body class="text-center">
	<div class="container">
		<div class="row">
			<div class="border rounded col-lg-3 col-md-5 col-sm-7 col-xl-3">
				<form action="login_cek.php" id="frm01" name="frm01" method="POST" class="form-signin">
					<img src="assets/images/login.jpg" width="100" height="100">
					<h1 class="h3 mb-3 font-weight-normal">Please Sign In</h1>
					<?php if (isset($_SESSION['gagal'])) : ?>
						<?php if ($_SESSION['gagal'] >= 3) : ?>
							<div class="alert alert-danger alert-dismissible fade show" role="alert">
								GAGAL Login ke-<?= $_SESSION['gagal'] ?>!!
								<button type="button" class="close" data-dismiss="alert" aria-label="close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
						<?php endif; ?>
					<?php endif; ?>
					<div class="form-group">
						<label for="inputEmail" class="sr-only">Email address</label>
						<input type="email" name="inEmail" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
					</div>
					<div class="form-group">
						<label for="inputPassword" class="sr-only">Password</label>
						<input type="password" name="inPassword" id="inputPassword" class="form-control" placeholder="Password" required autofocus>
					</div>

					<button class="btn btn-lg btn-primary btn-block" type="submit">Sign In</button>
					<p class="mt-5 mb-3 text-muted">&copy; SIBW 2019-2020</p>
				</form>
			</div>
		</div>
	</div>

	<script src="assets/jquery/jquery.min.js"></script>
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script>
		window.setTimeout(() => {
			$('.alert').fadeTo(500, 0).slideUp(500, () => {
				$(this.remove())
			})
		}, 2000)
	</script>
</body>
</html>