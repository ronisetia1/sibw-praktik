<?php

/**
 *
 */
class Validation
{
	protected $db;

	public function __construct($db)
	{
		$this->db = $db;
	}

	public function index()
	{
        $query = "SELECT * FROM pembelian";
        $result = $this->db->query($query);

        $data = array();
        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            $data[] = $row;
        }

        echo json_encode([
        	'success' => true,
        	'data' => $data
        ]);
	}

    public function search($data)
    {
		$text   = $data['text'];

		$query  = "SELECT * FROM pembelian WHERE no_pem LIKE '%$text%' OR tgl_pem LIKE '%$text%' OR usr_pem LIKE '%$text%' OR norek_pem LIKE '%$text%' OR nmrek_pem LIKE '%$text%' OR bankrek_pem LIKE '%$text%' OR sts_pem LIKE '%$text%'";
		$result = $this->db->query($query);

		$data   = array();
        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            $data[] = $row;
        }

        echo json_encode([
            'success' => true,
            'data' => $data
        ]);
    }

    public function statusUp($data)
    {
    	$id = $data['id'];

    	switch ($data['status']) {
    		case 'VALIDATION':
    			$status = 'PACKING';
    			break;
    		case 'PACKING':
    			$status = 'SENDING';
    			break;
    		case 'SENDING':
    			$status = 'COMPELETE';
    			break;
    		default:
    			$status = 'VALIDATION';
    			break;
    	}

		$query  = "UPDATE pembelian SET sts_pem='$status' WHERE id='$id'";
		$result = $this->db->query($query);

        echo json_encode([
            'success' => true,
            'message' => 'Pembelian updated!'
        ]);
    }
}