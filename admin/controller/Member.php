<?php

/**
 *
 */
class Member
{
	protected $db;

	public function __construct($db)
	{
		$this->db = $db;
	}

	public function index()
	{
        $query = "SELECT * FROM member";
        $result = $this->db->query($query);

        $data = array();
        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            $data[] = $row;
        }

        echo json_encode([
        	'success' => true,
        	'data' => $data
        ]);
	}

    private function upload($image)
    {
        $ekstensi_diperbolehkan = ['png','jpg', 'jpeg'];
        $nama                   = $image['name'];
        $x                      = explode('.', $nama);
        $ekstensi               = strtolower(end($x));
        $ukuran                 = $image['size'];
        $file_tmp               = $image['tmp_name'];
        $newName                = 'member_img/' . date('YmdHis') . '.' . $ekstensi;
        if (in_array($ekstensi, $ekstensi_diperbolehkan)) {
                move_uploaded_file($file_tmp, '../' . $newName);

                return $newName;
        } else {
            return false;
        }
    }

	public function store($data, $image)
	{
        $nm_usr    = $data['nm_usr'];
        $pas_usr   = $data['pas_usr'];
        $email_usr = $data['email_usr'];
        $almt_usr  = $data['almt_usr'];
        $tlp       = $data['tlp'];

        $newName   = $this->upload($image);

        if (!$newName) {
            die();
        }

		try {
            $query = "INSERT INTO member (nm_usr, pas_usr, email_usr, almt_usr, tlp, photo) VALUES ('$nm_usr', '$pas_usr', '$email_usr', '$almt_usr', '$tlp', '$newName')";
            $result = $this->db->query($query);

            echo json_encode([
            	'success' => true,
            	'message' => 'Member created!'
            ]);
        } catch (\Throwable $th) {
            print_r($th);
            die;
        }
        die();
	}

	public function edit($data)
	{
		$id     = $data['id'];
		$query  = "SELECT * FROM member WHERE id = $id";
		$result = $this->db->query($query);

        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            echo json_encode([
				'success' => true,
				'data' => $row
			]);
        }
	}

	public function update($data, $image)
	{
        $id        = $data['id'];
        $nm_usr    = $data['nm_usr'];
        $pas_usr   = $data['pas_usr'];
        $email_usr = $data['email_usr'];
        $almt_usr  = $data['almt_usr'];
        $tlp       = $data['tlp'];
        $image_old = $data['image_old'];

		try {
            if ($image != null) {
                $photo = $this->upload($image);

                if (!$photo) {
                    die();
                }

                unlink('../' . $image_old);
            } else {
                $photo = $image_old;
            }
            $query = "UPDATE member SET nm_usr='$nm_usr', pas_usr='$pas_usr', email_usr='$email_usr', almt_usr='$almt_usr', tlp='$tlp', photo='$photo' WHERE id='$id'";
            $result = $this->db->query($query);

            echo json_encode([
            	'success' => true,
            	'message' => 'Member updated!'
            ]);
        } catch (\Throwable $th) {
            print_r($th);
            die;
        }
        die();
	}

	public function delete($data)
	{
        $id = $data['id'];

		try {
            $query = "DELETE FROM member WHERE id='$id'";
            $result = $this->db->query($query);

            // unlink('../' . $br_gbr);

            echo json_encode([
            	'success' => true,
            	'message' => 'Product deleted!'
            ]);
        } catch (\Throwable $th) {
            print_r($th);
            die;
        }
        die();
	}

    public function search($data)
    {
        $text = $data['text'];

        $query = "SELECT * FROM member WHERE nm_usr LIKE '%$text%' OR email_usr LIKE '%$text%'";
        $result = $this->db->query($query);

        $data = array();
        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            $data[] = $row;
        }

        echo json_encode([
            'success' => true,
            'data' => $data
        ]);
    }
}